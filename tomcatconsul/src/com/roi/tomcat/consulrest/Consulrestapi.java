package com.roi.tomcat.consulrest;
 
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;

import org.apache.commons.collections.MultiHashMap;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
 
 
@SuppressWarnings("serial")
public class Consulrestapi extends HttpServlet
{
 
    public void init() throws ServletException
    {
    	
    	System.out.println("--------------------------------------------------------------------");
    	NetworkInterface ni;
		try {
			Thread.sleep(80000);
			ni = NetworkInterface.getByName("eth1");
			Enumeration<InetAddress> inetAddresses =  ni.getInetAddresses();
	        while(inetAddresses.hasMoreElements()) {
	            InetAddress ia = inetAddresses.nextElement();
	            if(!ia.isLinkLocalAddress()) {
	                System.out.println(ia.getHostAddress());
	                Consulrestapi.ServiceRegistrycall(ia.getHostAddress().toString());
	            }
	        }
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}   
		
    }
    
    public static void ServiceRegistrycall(String address)
    {
    	int x = (int)(Math.random() * 9);
	    x = x + 1;
	    String uuid = (x + "") + ( ((int)(Math.random()*1000)) + "" );
	    System.out.println(uuid);
    	try {

			Client client = Client.create();
			WebResource webResource = client.resource("http://172.16.1.14:8500/v1/catalog/register");	
			
					String input = "{  "+
					"  \"Datacenter\":\"dc1\","+
					"  \"Node\":\"PaasDemo\","+
					"    \"Address\": \""+address+"\",     "+
					"  \"Service\":{  "+
					"    \"ID\":\"qawebapp"+uuid+"\","+
					"    \"Service\":\"qatomcat\","+
					"    \"Tags\":[  "+
					"      \"qatomcat\","+
					"      \"v1\""+
					"    ],"+
					"    \"Address\": \""+address+"\",     "+
					"    \"TaggedAddresses\":{  "+
					"      \"wan\":\"127.0.0.1\""+
					"    },"+
					"    \"Port\":8080"+
					"  },"+
					"  \"Check\":{  "+
					"    \"Node\":\"PaasDemo\","+
					"    \"CheckID\":\"service:qatomcat\","+
					"    \"Name\":\"PaasDemo health check\","+
					"    \"Status\":\"passing\","+
					"    \"ServiceID\":\"qawebapp"+uuid+"\""+
					"  }"+
					"  }";
	                  System.out.println(input);
			ClientResponse response = webResource.type("application/json")
					.post(ClientResponse.class, input);
			
			if (response.getStatus() != 200) 
			{
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatus());
			}			
			String output = response.getEntity(String.class);
			}			        
            catch (Exception e) 
		    {
			  e.printStackTrace();
		    }
            finally
            { 
                System.out.println("rest of the code...");  
            }
}
}